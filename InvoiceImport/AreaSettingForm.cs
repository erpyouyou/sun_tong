﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Configuration;
using SHelper;
using System.Data.SqlClient;

namespace InvoiceImport
{
    public partial class AreaSettingForm : DevExpress.XtraEditors.XtraForm
    {
        public AreaSettingForm()
        {
            InitializeComponent();
        }

        string UFSystem = ConfigurationManager.ConnectionStrings["UFSystem"].ConnectionString;
        DataTable Table;

        private void gv_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        private void SaveBtn_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Save();
        }

        private void Save()
        {
            gv.CloseEditor();
            for (int i = 0; i < gv.RowCount; i++)
            {
                if (string.IsNullOrEmpty(gv.GetRowCellValue(i, aCode).ToString()))
                {
                    MessageBox.Show(string.Format("第{0}行,编码不能为空!", i + 1), "提示");
                    return;
                }
                if (string.IsNullOrEmpty(gv.GetRowCellValue(i, aName).ToString()))
                {
                    MessageBox.Show(string.Format("第{0}行,地区名称不能为空!", i + 1), "提示");
                    return;
                }
                if (SqlHelper.ExecuteDataTable(string.Format(@"select * from AreaMapping where AutoID<>{0} and aCode='{1}'"
                    , gv.GetRowCellValue(i, AutoID), gv.GetRowCellValue(i, aCode)), UFSystem, new SqlParameter()).Rows.Count > 0)
                {
                    MessageBox.Show(string.Format("第{0}行,编码重复!", i + 1), "提示");
                    return;
                }
                try
                {
                    SqlHelper.ExecuteNonQuery(string.Format(@"update AreaMapping set aCode='{0}',aName='{1}',
                aMode='{2}',aHeadRemark='{3}',aTaxRate='{4}' where AutoID={5}", gv.GetRowCellValue(i, aCode), gv.GetRowCellValue(i, aName)
                , gv.GetRowCellValue(i, aMode), gv.GetRowCellValue(i, aHeadRemark), gv.GetRowCellValue(i, aTaxRate), gv.GetRowCellValue(i, AutoID)), UFSystem, new SqlParameter());
                }
                catch (Exception ex)
                {
                    MessageBox.Show("保存失败!原因:" + ex.Message, "提示");
                    return;
                }
            }
            MessageBox.Show("保存成功!", "提示");
        }

        private void AreaSettingForm_Load(object sender, EventArgs e)
        {
            refresh();
        }

        private void RefreshBtn_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            refresh();
        }

        public void refresh()
        {
            Table = SqlHelper.ExecuteDataTable(string.Format(@"select * from AreaMapping"), UFSystem, new SqlParameter());
            gc.DataSource = Table;
        }

        private void DelBtn_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gv.RowCount == 0)
            {
                return;
            }
            SqlHelper.ExecuteNonQuery(string.Format(@"delete AreaMapping where AutoID={0}", gv.GetFocusedRowCellValue(AutoID)), UFSystem, new SqlParameter());
            refresh();
        }

        private void NewBtn_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Save();
            SqlHelper.ExecuteNonQuery(string.Format(@"insert into AreaMapping (aTaxRate) values(0)"), UFSystem, new SqlParameter());
            refresh();
        }
    }
}