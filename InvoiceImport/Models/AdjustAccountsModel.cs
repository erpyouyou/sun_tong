﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InvoiceImport.Models
{
    /// <summary>
    /// 辅助核算
    /// </summary>
    public class AdjustAccountsModel
    {
        /// <summary>
        /// 科目编码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 项目核算
        /// </summary>
        public string item { get; set; }
        /// <summary>
        /// 部门核算
        /// </summary>
        public string dept { get; set; }
        /// <summary>
        /// 个人核算
        /// </summary>
        public string person { get; set; }
        /// <summary>
        /// 客户核算
        /// </summary>
        public string cus { get; set; }
        /// <summary>
        /// 供应商核算
        /// </summary>
        public string sup { get; set; }
    }
}
