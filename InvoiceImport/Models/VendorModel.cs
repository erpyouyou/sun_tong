﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InvoiceImport.Models
{
    /// <summary>
    /// 供应商
    /// </summary>
    public class VendorModel
    {
        /// <summary>
        /// 供应商编码
        /// </summary>
        public string cVenCode { get; set; }
        /// <summary>
        /// 供应商名称
        /// </summary>
        public string cVenName { get; set; }
    }
}
