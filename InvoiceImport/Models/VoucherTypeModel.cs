﻿namespace InvoiceImport.Models
{
    /// <summary>
    /// 凭证类别
    /// </summary>
    public class VoucherTypeModel
    {
        /// <summary>
        /// 凭证类别  字
        /// </summary>
        public string Csign { get; set; }
        /// <summary>
        /// 凭证类别排序号 
        /// </summary>
        public int Isignseq { get; set; }
        /// <summary>
        /// 凭证类别 名称
        /// </summary>
        public string Ctext { get; set; }
    }
}
