﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InvoiceImport.Models
{
    /// <summary>
    /// 客户
    /// </summary>
    public class CustomersModel
    {
        /// <summary>
        /// 客户编码
        /// </summary>
        public string cCusCode { get; set; }
        /// <summary>
        /// 客户名称
        /// </summary>
        public string cCusName { get; set; }
    }
}
