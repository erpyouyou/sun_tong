﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InvoiceImport.Models
{
    /// <summary>
    /// 项目
    /// </summary>
    public class ProjectModel
    {
        /// <summary>
        /// 项目编码
        /// </summary>
        public string citemcode { get; set; }
        /// <summary>
        /// 项目名称
        /// </summary>
        public string citemname { get; set; }
        /// <summary>
        /// 是否结算
        /// </summary>
        public int bclose { get; set; }
        /// <summary>
        /// 项目大类编码
        /// </summary>
        public string citemccode { get; set; }
    }
}
