﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InvoiceImport.Models
{
    /// <summary>
    /// 部门
    /// </summary>
    public class DepartmentModel
    {
        /// <summary>
        /// 部门编码
        /// </summary>
        public string cDepCode { get; set; }
        /// <summary>
        /// 部门名称
        /// </summary>
        public string cDepName { get; set; }
    }
}
