﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InvoiceImport.Models
{
    /// <summary>
    /// 科目编码
    /// </summary>
    public class CodeModel
    {
        /// <summary>
        /// 科目编码
        /// </summary>
        public string Ccode { get; set; }
        /// <summary>
        /// 科目名称
        /// </summary>
        public string Ccode_name { get; set; }
        /// <summary>
        /// 借贷方向;True借;False贷
        /// </summary>
        public bool Bproperty { get; set; }
        /// <summary>
        /// 是否末级科目
        /// </summary>
        public bool Bend { get; set; }
        /// <summary>
        /// 是否个人往来核算
        /// </summary>
        public int Bperson { get; set; }
        /// <summary>
        /// 是否客户往来核算
        /// </summary>
        public int Bcus { get; set; }
        /// <summary>
        /// 是否供应商往来核算
        /// </summary>
        public int Bsup { get; set; }
        /// <summary>
        /// 是否项目核算
        /// </summary>
        public int Bitem { get; set; }
        /// <summary>
        /// 是否部门核算
        /// </summary>
        public int Bdept { get; set; }
    }
}
