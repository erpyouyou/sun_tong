﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvoiceImport.Models
{
    public static class AppConfig
    {
        /// <summary>
        /// 四舍五入 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="digits"></param>
        /// <returns></returns>
        public static decimal ToMathRound(this decimal obj, int digits)
        {
            return Math.Round(obj, digits);
        }
    }
}
