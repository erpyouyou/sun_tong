﻿using System;

namespace InvoiceImport.Models
{
    /// <summary>
    /// U8凭证实体
    /// </summary>
    public class GlAccvouchModel
    {
        /// <summary>
        /// 会计期间
        /// </summary>
        public int Iperiod { get; set; }
        /// <summary>
        /// 凭证类型
        /// </summary>
        public string Csign { get; set; }
        /// <summary>
        /// 凭证类别排序号 
        /// </summary>
        public int Isignseq { get; set; }
        /// <summary>
        /// 凭证编号
        /// </summary>
        public int Ino_id { get; set; }
        /// <summary>
        /// 行号 
        /// </summary>
        public int Inid { get; set; }
        /// <summary>
        /// 附单据数
        /// </summary>
        public string Idoc { get; set; }
        /// <summary>
        /// 制单日期 
        /// </summary>
        public DateTime Dbill_date { get; set; }
        /// <summary>
        /// 制单人
        /// </summary>
        public string Cbill { get; set; }
        /// <summary>
        /// 记账标志 
        /// </summary>
        public string Ibook { get; set; }
        /// <summary>
        /// 摘要
        /// </summary>
        public string Cdigest { get; set; }
        /// <summary>
        /// 科目编码 
        /// </summary>
        public string Ccode { get; set; }
        /// <summary>
        /// 币种名称
        /// </summary>
        public string Cexch_name { get; set; }
        /// <summary>
        /// 借方金额
        /// </summary>
        public decimal Md { get; set; }
        /// <summary>
        /// 贷方金额
        /// </summary>
        public decimal Mc { get; set; }
        /// <summary>
        /// 外币借方金额 
        /// </summary>
        public decimal Md_f { get; set; }
        /// <summary>
        /// 外币贷方金额 
        /// </summary>
        public decimal Mc_f { get; set; }
        /// <summary>
        /// 汇率
        /// </summary>
        public decimal Nfrat { get; set; }
        /// <summary>
        /// 数量借方
        /// </summary>
        public decimal Nd_s { get; set; }
        /// <summary>
        /// 数量贷方 
        /// </summary>
        public decimal Nc_s { get; set; }
        /// <summary>
        /// 凭证的会计年度
        /// </summary>
        public int Iyear { get; set; }
        /// <summary>
        /// 包括年度的会计期间
        /// 格式:yyyyMM  例:202012
        /// </summary>
        public string IYPeriod { get; set; }
        /// <summary>
        /// 部门编码 
        /// </summary>
        public string Cdept_id { get; set; }
        /// <summary>
        /// 职员编码 
        /// </summary>
        public string Cperson_id { get; set; }
        /// <summary>
        /// 客户编码 
        /// </summary>
        public string Ccus_id { get; set; }
        /// <summary>
        /// 供应商编码 
        /// </summary>
        public string Csup_id { get; set; }
        /// <summary>
        /// 项目编码 
        /// </summary>
        public string Citem_id { get; set; }
        /// <summary>
        /// 项目大类编码
        /// </summary>
        public string Citem_class { get; set; }
        /// <summary>
        /// 业务员
        /// </summary>
        public string Cname { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime Doutbilldate { get; set; }
        /// <summary>
        /// 默认为1
        /// </summary>
        public int Bvouchedit { get; set; } = 1;
        /// <summary>
        /// 默认为1
        /// </summary>
        public int Bvalueedit { get; set; } = 1;
        /// <summary>
        /// 分录科目是否可修改 
        /// 默认为1
        /// </summary>
        public int Bcodeedit { get; set; } = 1;
        /// <summary>
        /// 分录往来项是否可修改 
        /// 默认为1
        /// </summary>
        public int BPCSedit { get; set; } = 1;
        /// <summary>
        /// 分录部门是否可修改 
        /// 默认为1
        /// </summary>
        public int BDeptedit { get; set; } = 1;
        /// <summary>
        /// 默认为1
        /// </summary>
        public int BItemedit { get; set; } = 1;
    }
}
