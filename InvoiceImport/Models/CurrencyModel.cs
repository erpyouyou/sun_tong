﻿namespace InvoiceImport.Models
{
    /// <summary>
    /// 币种
    /// </summary>
    public class CurrencyModel
    {
        /// <summary>
        /// 币种编码
        /// </summary>
        public string Cexch_code { get; set; }
        /// <summary>
        /// 币种名称
        /// </summary>
        public string Cexch_name { get; set; }
        /// <summary>
        /// 类型 2:固定汇率
        /// </summary>
        public string itype { get; set; }
        /// <summary>
        /// 会计月
        /// </summary>
        public int iperiod { get; set; }
        /// <summary>
        /// 会计年度
        /// </summary>
        public int iYear { get; set; }
        /// <summary>
        /// 汇率
        /// </summary>
        public decimal nflat { get; set; }
    }
}
