﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InvoiceImport.Models
{
    /// <summary>
    /// 人员
    /// </summary>
    public class PersonModel
    {
        /// <summary>
        /// 人员编码
        /// </summary>
        public string cPsn_Num { get; set; }
        /// <summary>
        /// 人员名称
        /// </summary>
        public string cPsn_Name { get; set; }
        /// <summary>
        /// 部门编码
        /// </summary>
        public string cDept_num { get; set; }
    }
}
