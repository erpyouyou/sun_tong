﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using SHelper;
using System.Data.SqlClient;
using DevExpress.XtraSplashScreen;
using DevExpress.XtraWaitForm;
using System.Configuration;

namespace InvoiceImport
{
    public partial class YSYF_Form : DevExpress.XtraEditors.XtraForm
    {
        public YSYF_Form()
        {
            InitializeComponent();
        }
        string sdate = "", edate = "";
        string con = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        public string ServerText { get; set; }

        private void SelectBtn_Click(object sender, EventArgs e)
        {
            var wf = new SplashScreenManager(this, typeof(DemoWaitForm), true, true);//进程
            wf.ShowWaitForm();
            wf.SetWaitFormCaption("正在查询...");
            #region 设置日期
            if (string.IsNullOrEmpty(SDate.Text))
            {
                sdate = "1900-01-01";
            }
            else
            {
                sdate = SDate.Text;
            }
            if (string.IsNullOrEmpty(EDate.Text))
            {
                edate = "2900-01-01";
            }
            else
            {
                edate = EDate.Text;
            }
            #endregion
            string cus = "";
            if (!string.IsNullOrEmpty(CustomerInfo.Text))
            {
                cus = string.Format(" and (a.ccus_id like '%{0}%' or b.cCusName like '%{0}%')", CustomerInfo.Text);
            }
            DataTable table = SqlHelper.ExecuteDataTable(
                string.Format(@"select * from gl_accvouch as a 
                                left join Customer as b on a.ccus_id=b.cCusCode 
                                left join code as c on a.ccode=c.ccode 
                                where a.dbill_date>='{0}' and a.dbill_date <='{1}' 
                                and c.iyear='{2}' {3}"
                , sdate, edate, DateTime.Today.Year, cus
                )
                , con, new SqlParameter());
            gc1.DataSource = table;
            if (wf.IsSplashFormVisible) wf.CloseWaitForm();//关闭进程
        }

        private void YSYF_Form_Load(object sender, EventArgs e)
        {
            con = con.Replace("UFDATA_999_2013", ServerText);
        }
    }
}