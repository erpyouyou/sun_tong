﻿namespace InvoiceImport
{
    partial class ReportForm2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportForm2));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.cMonth = new System.Windows.Forms.ComboBox();
            this.cYear = new System.Windows.Forms.ComboBox();
            this.AreaSetting = new DevExpress.XtraEditors.SimpleButton();
            this.ExPortBtn = new DevExpress.XtraEditors.SimpleButton();
            this.SelectBtn = new DevExpress.XtraEditors.SimpleButton();
            this.gc = new DevExpress.XtraGrid.GridControl();
            this.gv = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.项目名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.发生日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.存货编码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.币种 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.汇率 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.税率 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.成本数量 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.成本原币无税金额 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.成本原币价税合计 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.成本本币无税金额 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.成本本币价税合计 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.收入数量 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.收入原币无税金额 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.收入原币价税合计 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.收入本币无税金额 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.收入本币价税合计 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.原币无税利润 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.原币含税利润 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.本币无税利润 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.本币含税利润 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.地区 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Mode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Head = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HeadRemark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TaxRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.上月预估收入 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.当月预估收入 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.不含税收入含预估 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.上月预估成本 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.当月预估成本 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.不含税成本含预估 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.上月预估利润 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.当月预估利润 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.不含税利润含预估 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.cMonth);
            this.layoutControl1.Controls.Add(this.cYear);
            this.layoutControl1.Controls.Add(this.AreaSetting);
            this.layoutControl1.Controls.Add(this.ExPortBtn);
            this.layoutControl1.Controls.Add(this.SelectBtn);
            this.layoutControl1.Controls.Add(this.gc);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(961, 471);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // cMonth
            // 
            this.cMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cMonth.FormattingEnabled = true;
            this.cMonth.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.cMonth.Location = new System.Drawing.Point(112, 12);
            this.cMonth.Name = "cMonth";
            this.cMonth.Size = new System.Drawing.Size(51, 22);
            this.cMonth.TabIndex = 12;
            // 
            // cYear
            // 
            this.cYear.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cYear.FormattingEnabled = true;
            this.cYear.Location = new System.Drawing.Point(12, 12);
            this.cYear.Name = "cYear";
            this.cYear.Size = new System.Drawing.Size(81, 22);
            this.cYear.TabIndex = 11;
            // 
            // AreaSetting
            // 
            this.AreaSetting.Location = new System.Drawing.Point(372, 12);
            this.AreaSetting.Name = "AreaSetting";
            this.AreaSetting.Size = new System.Drawing.Size(76, 22);
            this.AreaSetting.StyleController = this.layoutControl1;
            this.AreaSetting.TabIndex = 10;
            this.AreaSetting.Text = "地区维护";
            this.AreaSetting.Click += new System.EventHandler(this.AreaSetting_Click);
            // 
            // ExPortBtn
            // 
            this.ExPortBtn.Location = new System.Drawing.Point(282, 12);
            this.ExPortBtn.Name = "ExPortBtn";
            this.ExPortBtn.Size = new System.Drawing.Size(76, 22);
            this.ExPortBtn.StyleController = this.layoutControl1;
            this.ExPortBtn.TabIndex = 9;
            this.ExPortBtn.Text = "导出";
            this.ExPortBtn.Click += new System.EventHandler(this.ExPortBtn_Click);
            // 
            // SelectBtn
            // 
            this.SelectBtn.Location = new System.Drawing.Point(192, 12);
            this.SelectBtn.Name = "SelectBtn";
            this.SelectBtn.Size = new System.Drawing.Size(76, 22);
            this.SelectBtn.StyleController = this.layoutControl1;
            this.SelectBtn.TabIndex = 8;
            this.SelectBtn.Text = "查询";
            this.SelectBtn.Click += new System.EventHandler(this.SelectBtn_Click);
            // 
            // gc
            // 
            this.gc.Location = new System.Drawing.Point(12, 38);
            this.gc.MainView = this.gv;
            this.gc.Name = "gc";
            this.gc.Size = new System.Drawing.Size(937, 421);
            this.gc.TabIndex = 4;
            this.gc.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv});
            // 
            // gv
            // 
            this.gv.ColumnPanelRowHeight = 35;
            this.gv.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.项目名称,
            this.发生日期,
            this.存货编码,
            this.币种,
            this.汇率,
            this.税率,
            this.成本数量,
            this.成本原币无税金额,
            this.成本原币价税合计,
            this.成本本币无税金额,
            this.成本本币价税合计,
            this.收入数量,
            this.收入原币无税金额,
            this.收入原币价税合计,
            this.收入本币无税金额,
            this.收入本币价税合计,
            this.原币无税利润,
            this.原币含税利润,
            this.本币无税利润,
            this.本币含税利润,
            this.地区,
            this.Mode,
            this.Head,
            this.HeadRemark,
            this.TaxRate,
            this.上月预估收入,
            this.当月预估收入,
            this.不含税收入含预估,
            this.上月预估成本,
            this.当月预估成本,
            this.不含税成本含预估,
            this.上月预估利润,
            this.当月预估利润,
            this.不含税利润含预估});
            this.gv.GridControl = this.gc;
            this.gv.IndicatorWidth = 50;
            this.gv.Name = "gv";
            this.gv.OptionsFind.AlwaysVisible = true;
            this.gv.OptionsFind.FindNullPrompt = "请输入查询条件...";
            this.gv.OptionsFind.ShowClearButton = false;
            this.gv.OptionsFind.ShowCloseButton = false;
            this.gv.OptionsFind.ShowFindButton = false;
            this.gv.OptionsMenu.EnableColumnMenu = false;
            this.gv.OptionsMenu.EnableFooterMenu = false;
            this.gv.OptionsMenu.EnableGroupPanelMenu = false;
            this.gv.OptionsView.ColumnAutoWidth = false;
            this.gv.OptionsView.EnableAppearanceEvenRow = true;
            this.gv.OptionsView.ShowGroupPanel = false;
            this.gv.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.gv_CustomDrawRowIndicator);
            // 
            // 项目名称
            // 
            this.项目名称.AppearanceHeader.Options.UseTextOptions = true;
            this.项目名称.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.项目名称.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.项目名称.Caption = "项目名称";
            this.项目名称.FieldName = "项目名称";
            this.项目名称.Name = "项目名称";
            this.项目名称.OptionsColumn.AllowEdit = false;
            this.项目名称.Width = 140;
            // 
            // 发生日期
            // 
            this.发生日期.AppearanceHeader.Options.UseTextOptions = true;
            this.发生日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.发生日期.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.发生日期.Caption = "发生日期";
            this.发生日期.FieldName = "发生日期";
            this.发生日期.Name = "发生日期";
            this.发生日期.OptionsColumn.AllowEdit = false;
            this.发生日期.Width = 100;
            // 
            // 存货编码
            // 
            this.存货编码.AppearanceHeader.Options.UseTextOptions = true;
            this.存货编码.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.存货编码.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.存货编码.Caption = "存货编码";
            this.存货编码.FieldName = "存货编码";
            this.存货编码.Name = "存货编码";
            this.存货编码.OptionsColumn.AllowEdit = false;
            this.存货编码.Width = 80;
            // 
            // 币种
            // 
            this.币种.AppearanceHeader.Options.UseTextOptions = true;
            this.币种.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.币种.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.币种.Caption = "币种";
            this.币种.FieldName = "币种";
            this.币种.Name = "币种";
            this.币种.OptionsColumn.AllowEdit = false;
            this.币种.Width = 70;
            // 
            // 汇率
            // 
            this.汇率.AppearanceHeader.Options.UseTextOptions = true;
            this.汇率.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.汇率.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.汇率.Caption = "汇率";
            this.汇率.DisplayFormat.FormatString = "{0:N}";
            this.汇率.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.汇率.FieldName = "汇率";
            this.汇率.Name = "汇率";
            this.汇率.OptionsColumn.AllowEdit = false;
            // 
            // 税率
            // 
            this.税率.AppearanceHeader.Options.UseTextOptions = true;
            this.税率.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.税率.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.税率.Caption = "税率";
            this.税率.DisplayFormat.FormatString = "{0:N}";
            this.税率.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.税率.FieldName = "税率";
            this.税率.Name = "税率";
            this.税率.OptionsColumn.AllowEdit = false;
            // 
            // 成本数量
            // 
            this.成本数量.AppearanceHeader.Options.UseTextOptions = true;
            this.成本数量.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.成本数量.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.成本数量.Caption = "成本数量";
            this.成本数量.DisplayFormat.FormatString = "{0:N}";
            this.成本数量.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.成本数量.FieldName = "成本数量";
            this.成本数量.Name = "成本数量";
            this.成本数量.OptionsColumn.AllowEdit = false;
            // 
            // 成本原币无税金额
            // 
            this.成本原币无税金额.AppearanceHeader.Options.UseTextOptions = true;
            this.成本原币无税金额.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.成本原币无税金额.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.成本原币无税金额.Caption = "成本原币无税金额";
            this.成本原币无税金额.DisplayFormat.FormatString = "{0:N}";
            this.成本原币无税金额.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.成本原币无税金额.FieldName = "成本原币无税金额";
            this.成本原币无税金额.Name = "成本原币无税金额";
            this.成本原币无税金额.OptionsColumn.AllowEdit = false;
            this.成本原币无税金额.Width = 100;
            // 
            // 成本原币价税合计
            // 
            this.成本原币价税合计.AppearanceHeader.Options.UseTextOptions = true;
            this.成本原币价税合计.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.成本原币价税合计.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.成本原币价税合计.Caption = "成本原币价税合计";
            this.成本原币价税合计.DisplayFormat.FormatString = "{0:N}";
            this.成本原币价税合计.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.成本原币价税合计.FieldName = "成本原币价税合计";
            this.成本原币价税合计.Name = "成本原币价税合计";
            this.成本原币价税合计.OptionsColumn.AllowEdit = false;
            this.成本原币价税合计.Width = 100;
            // 
            // 成本本币无税金额
            // 
            this.成本本币无税金额.AppearanceHeader.Options.UseTextOptions = true;
            this.成本本币无税金额.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.成本本币无税金额.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.成本本币无税金额.Caption = "成本本币无税金额";
            this.成本本币无税金额.DisplayFormat.FormatString = "{0:N}";
            this.成本本币无税金额.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.成本本币无税金额.FieldName = "成本本币无税金额";
            this.成本本币无税金额.Name = "成本本币无税金额";
            this.成本本币无税金额.OptionsColumn.AllowEdit = false;
            this.成本本币无税金额.Visible = true;
            this.成本本币无税金额.VisibleIndex = 9;
            this.成本本币无税金额.Width = 100;
            // 
            // 成本本币价税合计
            // 
            this.成本本币价税合计.AppearanceHeader.Options.UseTextOptions = true;
            this.成本本币价税合计.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.成本本币价税合计.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.成本本币价税合计.Caption = "成本本币价税合计";
            this.成本本币价税合计.DisplayFormat.FormatString = "{0:N}";
            this.成本本币价税合计.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.成本本币价税合计.FieldName = "成本本币价税合计";
            this.成本本币价税合计.Name = "成本本币价税合计";
            this.成本本币价税合计.OptionsColumn.AllowEdit = false;
            this.成本本币价税合计.Width = 100;
            // 
            // 收入数量
            // 
            this.收入数量.AppearanceHeader.Options.UseTextOptions = true;
            this.收入数量.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.收入数量.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.收入数量.Caption = "收入数量";
            this.收入数量.DisplayFormat.FormatString = "{0:N}";
            this.收入数量.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.收入数量.FieldName = "收入数量";
            this.收入数量.Name = "收入数量";
            this.收入数量.OptionsColumn.AllowEdit = false;
            // 
            // 收入原币无税金额
            // 
            this.收入原币无税金额.AppearanceHeader.Options.UseTextOptions = true;
            this.收入原币无税金额.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.收入原币无税金额.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.收入原币无税金额.Caption = "收入原币无税金额";
            this.收入原币无税金额.DisplayFormat.FormatString = "{0:N}";
            this.收入原币无税金额.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.收入原币无税金额.FieldName = "收入原币无税金额";
            this.收入原币无税金额.Name = "收入原币无税金额";
            this.收入原币无税金额.OptionsColumn.AllowEdit = false;
            this.收入原币无税金额.Width = 100;
            // 
            // 收入原币价税合计
            // 
            this.收入原币价税合计.AppearanceHeader.Options.UseTextOptions = true;
            this.收入原币价税合计.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.收入原币价税合计.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.收入原币价税合计.Caption = "收入原币价税合计";
            this.收入原币价税合计.DisplayFormat.FormatString = "{0:N}";
            this.收入原币价税合计.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.收入原币价税合计.FieldName = "收入原币价税合计";
            this.收入原币价税合计.Name = "收入原币价税合计";
            this.收入原币价税合计.OptionsColumn.AllowEdit = false;
            this.收入原币价税合计.Width = 100;
            // 
            // 收入本币无税金额
            // 
            this.收入本币无税金额.AppearanceHeader.Options.UseTextOptions = true;
            this.收入本币无税金额.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.收入本币无税金额.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.收入本币无税金额.Caption = "收入本币无税金额";
            this.收入本币无税金额.DisplayFormat.FormatString = "{0:N}";
            this.收入本币无税金额.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.收入本币无税金额.FieldName = "收入本币无税金额";
            this.收入本币无税金额.Name = "收入本币无税金额";
            this.收入本币无税金额.OptionsColumn.AllowEdit = false;
            this.收入本币无税金额.Visible = true;
            this.收入本币无税金额.VisibleIndex = 5;
            this.收入本币无税金额.Width = 100;
            // 
            // 收入本币价税合计
            // 
            this.收入本币价税合计.AppearanceHeader.Options.UseTextOptions = true;
            this.收入本币价税合计.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.收入本币价税合计.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.收入本币价税合计.Caption = "收入本币价税合计";
            this.收入本币价税合计.DisplayFormat.FormatString = "{0:N}";
            this.收入本币价税合计.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.收入本币价税合计.FieldName = "收入本币价税合计";
            this.收入本币价税合计.Name = "收入本币价税合计";
            this.收入本币价税合计.OptionsColumn.AllowEdit = false;
            this.收入本币价税合计.Width = 100;
            // 
            // 原币无税利润
            // 
            this.原币无税利润.AppearanceHeader.Options.UseTextOptions = true;
            this.原币无税利润.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.原币无税利润.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.原币无税利润.Caption = "原币无税利润";
            this.原币无税利润.DisplayFormat.FormatString = "{0:N}";
            this.原币无税利润.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.原币无税利润.FieldName = "原币无税利润";
            this.原币无税利润.Name = "原币无税利润";
            this.原币无税利润.OptionsColumn.AllowEdit = false;
            this.原币无税利润.Width = 100;
            // 
            // 原币含税利润
            // 
            this.原币含税利润.AppearanceHeader.Options.UseTextOptions = true;
            this.原币含税利润.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.原币含税利润.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.原币含税利润.Caption = "原币含税利润";
            this.原币含税利润.DisplayFormat.FormatString = "{0:N}";
            this.原币含税利润.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.原币含税利润.FieldName = "原币含税利润";
            this.原币含税利润.Name = "原币含税利润";
            this.原币含税利润.OptionsColumn.AllowEdit = false;
            this.原币含税利润.Width = 100;
            // 
            // 本币无税利润
            // 
            this.本币无税利润.AppearanceHeader.Options.UseTextOptions = true;
            this.本币无税利润.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.本币无税利润.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.本币无税利润.Caption = "本币无税利润";
            this.本币无税利润.DisplayFormat.FormatString = "{0:N}";
            this.本币无税利润.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.本币无税利润.FieldName = "本币无税利润";
            this.本币无税利润.Name = "本币无税利润";
            this.本币无税利润.OptionsColumn.AllowEdit = false;
            this.本币无税利润.Visible = true;
            this.本币无税利润.VisibleIndex = 13;
            this.本币无税利润.Width = 100;
            // 
            // 本币含税利润
            // 
            this.本币含税利润.AppearanceHeader.Options.UseTextOptions = true;
            this.本币含税利润.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.本币含税利润.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.本币含税利润.Caption = "本币含税利润";
            this.本币含税利润.DisplayFormat.FormatString = "{0:N}";
            this.本币含税利润.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.本币含税利润.FieldName = "本币含税利润";
            this.本币含税利润.Name = "本币含税利润";
            this.本币含税利润.OptionsColumn.AllowEdit = false;
            this.本币含税利润.Width = 100;
            // 
            // 地区
            // 
            this.地区.AppearanceHeader.Options.UseTextOptions = true;
            this.地区.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.地区.Caption = "地区";
            this.地区.FieldName = "aName";
            this.地区.Name = "地区";
            this.地区.OptionsColumn.AllowEdit = false;
            this.地区.Visible = true;
            this.地区.VisibleIndex = 0;
            // 
            // Mode
            // 
            this.Mode.AppearanceHeader.Options.UseTextOptions = true;
            this.Mode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Mode.Caption = "Mode";
            this.Mode.FieldName = "aMode";
            this.Mode.Name = "Mode";
            this.Mode.OptionsColumn.AllowEdit = false;
            this.Mode.Visible = true;
            this.Mode.VisibleIndex = 1;
            // 
            // Head
            // 
            this.Head.AppearanceHeader.Options.UseTextOptions = true;
            this.Head.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Head.Caption = "Head";
            this.Head.FieldName = "aCode";
            this.Head.Name = "Head";
            this.Head.OptionsColumn.AllowEdit = false;
            this.Head.Visible = true;
            this.Head.VisibleIndex = 2;
            // 
            // HeadRemark
            // 
            this.HeadRemark.AppearanceHeader.Options.UseTextOptions = true;
            this.HeadRemark.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.HeadRemark.Caption = "HeadRemark";
            this.HeadRemark.FieldName = "aHeadRemark";
            this.HeadRemark.Name = "HeadRemark";
            this.HeadRemark.OptionsColumn.AllowEdit = false;
            this.HeadRemark.Visible = true;
            this.HeadRemark.VisibleIndex = 3;
            this.HeadRemark.Width = 175;
            // 
            // TaxRate
            // 
            this.TaxRate.AppearanceHeader.Options.UseTextOptions = true;
            this.TaxRate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TaxRate.Caption = "TaxRate";
            this.TaxRate.FieldName = "aTaxRate";
            this.TaxRate.Name = "TaxRate";
            this.TaxRate.OptionsColumn.AllowEdit = false;
            this.TaxRate.Visible = true;
            this.TaxRate.VisibleIndex = 4;
            // 
            // 上月预估收入
            // 
            this.上月预估收入.AppearanceHeader.Options.UseTextOptions = true;
            this.上月预估收入.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.上月预估收入.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.上月预估收入.Caption = "上月预估收入";
            this.上月预估收入.DisplayFormat.FormatString = "{0:N}";
            this.上月预估收入.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.上月预估收入.FieldName = "上月预估收入";
            this.上月预估收入.Name = "上月预估收入";
            this.上月预估收入.OptionsColumn.AllowEdit = false;
            this.上月预估收入.Visible = true;
            this.上月预估收入.VisibleIndex = 6;
            this.上月预估收入.Width = 100;
            // 
            // 当月预估收入
            // 
            this.当月预估收入.AppearanceHeader.Options.UseTextOptions = true;
            this.当月预估收入.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.当月预估收入.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.当月预估收入.Caption = "当月预估收入";
            this.当月预估收入.DisplayFormat.FormatString = "{0:N}";
            this.当月预估收入.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.当月预估收入.FieldName = "当月预估收入";
            this.当月预估收入.Name = "当月预估收入";
            this.当月预估收入.OptionsColumn.AllowEdit = false;
            this.当月预估收入.Visible = true;
            this.当月预估收入.VisibleIndex = 7;
            this.当月预估收入.Width = 100;
            // 
            // 不含税收入含预估
            // 
            this.不含税收入含预估.AppearanceHeader.Options.UseTextOptions = true;
            this.不含税收入含预估.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.不含税收入含预估.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.不含税收入含预估.Caption = "不含税收入(含预估)";
            this.不含税收入含预估.DisplayFormat.FormatString = "{0:N}";
            this.不含税收入含预估.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.不含税收入含预估.FieldName = "不含税收入含预估";
            this.不含税收入含预估.Name = "不含税收入含预估";
            this.不含税收入含预估.OptionsColumn.AllowEdit = false;
            this.不含税收入含预估.Visible = true;
            this.不含税收入含预估.VisibleIndex = 8;
            this.不含税收入含预估.Width = 100;
            // 
            // 上月预估成本
            // 
            this.上月预估成本.AppearanceHeader.Options.UseTextOptions = true;
            this.上月预估成本.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.上月预估成本.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.上月预估成本.Caption = "上月预估成本";
            this.上月预估成本.DisplayFormat.FormatString = "{0:N}";
            this.上月预估成本.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.上月预估成本.FieldName = "上月预估成本";
            this.上月预估成本.Name = "上月预估成本";
            this.上月预估成本.OptionsColumn.AllowEdit = false;
            this.上月预估成本.Visible = true;
            this.上月预估成本.VisibleIndex = 10;
            this.上月预估成本.Width = 100;
            // 
            // 当月预估成本
            // 
            this.当月预估成本.AppearanceHeader.Options.UseTextOptions = true;
            this.当月预估成本.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.当月预估成本.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.当月预估成本.Caption = "当月预估成本";
            this.当月预估成本.DisplayFormat.FormatString = "{0:N}";
            this.当月预估成本.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.当月预估成本.FieldName = "当月预估成本";
            this.当月预估成本.Name = "当月预估成本";
            this.当月预估成本.OptionsColumn.AllowEdit = false;
            this.当月预估成本.Visible = true;
            this.当月预估成本.VisibleIndex = 11;
            this.当月预估成本.Width = 100;
            // 
            // 不含税成本含预估
            // 
            this.不含税成本含预估.AppearanceHeader.Options.UseTextOptions = true;
            this.不含税成本含预估.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.不含税成本含预估.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.不含税成本含预估.Caption = "不含税成本(含预估)";
            this.不含税成本含预估.DisplayFormat.FormatString = "{0:N}";
            this.不含税成本含预估.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.不含税成本含预估.FieldName = "不含税成本含预估";
            this.不含税成本含预估.Name = "不含税成本含预估";
            this.不含税成本含预估.OptionsColumn.AllowEdit = false;
            this.不含税成本含预估.Visible = true;
            this.不含税成本含预估.VisibleIndex = 12;
            this.不含税成本含预估.Width = 100;
            // 
            // 上月预估利润
            // 
            this.上月预估利润.AppearanceHeader.Options.UseTextOptions = true;
            this.上月预估利润.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.上月预估利润.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.上月预估利润.Caption = "上月预估利润";
            this.上月预估利润.DisplayFormat.FormatString = "{0:N}";
            this.上月预估利润.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.上月预估利润.FieldName = "上月预估利润";
            this.上月预估利润.Name = "上月预估利润";
            this.上月预估利润.OptionsColumn.AllowEdit = false;
            this.上月预估利润.Visible = true;
            this.上月预估利润.VisibleIndex = 14;
            this.上月预估利润.Width = 100;
            // 
            // 当月预估利润
            // 
            this.当月预估利润.AppearanceHeader.Options.UseTextOptions = true;
            this.当月预估利润.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.当月预估利润.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.当月预估利润.Caption = "当月预估利润";
            this.当月预估利润.DisplayFormat.FormatString = "{0:N}";
            this.当月预估利润.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.当月预估利润.FieldName = "当月预估利润";
            this.当月预估利润.Name = "当月预估利润";
            this.当月预估利润.OptionsColumn.AllowEdit = false;
            this.当月预估利润.Visible = true;
            this.当月预估利润.VisibleIndex = 15;
            this.当月预估利润.Width = 100;
            // 
            // 不含税利润含预估
            // 
            this.不含税利润含预估.AppearanceHeader.Options.UseTextOptions = true;
            this.不含税利润含预估.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.不含税利润含预估.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.不含税利润含预估.Caption = "不含税利润(含预估)";
            this.不含税利润含预估.DisplayFormat.FormatString = "{0:N}";
            this.不含税利润含预估.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.不含税利润含预估.FieldName = "不含税利润含预估";
            this.不含税利润含预估.Name = "不含税利润含预估";
            this.不含税利润含预估.OptionsColumn.AllowEdit = false;
            this.不含税利润含预估.Visible = true;
            this.不含税利润含预估.VisibleIndex = 16;
            this.不含税利润含预估.Width = 100;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem5,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem6,
            this.emptySpaceItem3,
            this.layoutControlItem7,
            this.emptySpaceItem4,
            this.layoutControlItem2,
            this.layoutControlItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(961, 471);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gc;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(941, 425);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.SelectBtn;
            this.layoutControlItem5.Location = new System.Drawing.Point(180, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(80, 26);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(80, 26);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(80, 26);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(440, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(501, 26);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(170, 0);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(10, 26);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 26);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(10, 26);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.ExPortBtn;
            this.layoutControlItem6.Location = new System.Drawing.Point(270, 0);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(80, 26);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(80, 26);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(80, 26);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(260, 0);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(10, 26);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 26);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(10, 26);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.AreaSetting;
            this.layoutControlItem7.Location = new System.Drawing.Point(360, 0);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(80, 26);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(80, 26);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(80, 26);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(350, 0);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(10, 26);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 26);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(10, 26);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.cYear;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(100, 26);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(100, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(100, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "年";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Right;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(12, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.cMonth;
            this.layoutControlItem3.Location = new System.Drawing.Point(100, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(70, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(70, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(70, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "月";
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Right;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(12, 14);
            // 
            // ReportForm2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(961, 471);
            this.Controls.Add(this.layoutControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ReportForm2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "收入成本预估一览表";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ReportForm_FormClosed);
            this.Load += new System.EventHandler(this.ReportForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraGrid.GridControl gc;
        private DevExpress.XtraGrid.Views.Grid.GridView gv;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.SimpleButton SelectBtn;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraGrid.Columns.GridColumn 项目名称;
        private DevExpress.XtraGrid.Columns.GridColumn 发生日期;
        private DevExpress.XtraGrid.Columns.GridColumn 存货编码;
        private DevExpress.XtraGrid.Columns.GridColumn 币种;
        private DevExpress.XtraGrid.Columns.GridColumn 汇率;
        private DevExpress.XtraGrid.Columns.GridColumn 税率;
        private DevExpress.XtraGrid.Columns.GridColumn 成本数量;
        private DevExpress.XtraGrid.Columns.GridColumn 成本原币无税金额;
        private DevExpress.XtraGrid.Columns.GridColumn 成本原币价税合计;
        private DevExpress.XtraGrid.Columns.GridColumn 成本本币无税金额;
        private DevExpress.XtraGrid.Columns.GridColumn 成本本币价税合计;
        private DevExpress.XtraGrid.Columns.GridColumn 收入数量;
        private DevExpress.XtraGrid.Columns.GridColumn 收入原币无税金额;
        private DevExpress.XtraGrid.Columns.GridColumn 收入原币价税合计;
        private DevExpress.XtraGrid.Columns.GridColumn 收入本币无税金额;
        private DevExpress.XtraGrid.Columns.GridColumn 收入本币价税合计;
        private DevExpress.XtraGrid.Columns.GridColumn 原币无税利润;
        private DevExpress.XtraGrid.Columns.GridColumn 原币含税利润;
        private DevExpress.XtraGrid.Columns.GridColumn 本币无税利润;
        private DevExpress.XtraGrid.Columns.GridColumn 本币含税利润;
        private DevExpress.XtraEditors.SimpleButton ExPortBtn;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.SimpleButton AreaSetting;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraGrid.Columns.GridColumn 地区;
        private DevExpress.XtraGrid.Columns.GridColumn Mode;
        private DevExpress.XtraGrid.Columns.GridColumn Head;
        private DevExpress.XtraGrid.Columns.GridColumn HeadRemark;
        private DevExpress.XtraGrid.Columns.GridColumn TaxRate;
        private DevExpress.XtraGrid.Columns.GridColumn 上月预估收入;
        private DevExpress.XtraGrid.Columns.GridColumn 当月预估收入;
        private DevExpress.XtraGrid.Columns.GridColumn 不含税收入含预估;
        private DevExpress.XtraGrid.Columns.GridColumn 上月预估成本;
        private DevExpress.XtraGrid.Columns.GridColumn 当月预估成本;
        private DevExpress.XtraGrid.Columns.GridColumn 不含税成本含预估;
        private DevExpress.XtraGrid.Columns.GridColumn 上月预估利润;
        private DevExpress.XtraGrid.Columns.GridColumn 当月预估利润;
        private DevExpress.XtraGrid.Columns.GridColumn 不含税利润含预估;
        private System.Windows.Forms.ComboBox cMonth;
        private System.Windows.Forms.ComboBox cYear;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
    }
}